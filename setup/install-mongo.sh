#! /bin/sh

mkdir .tmp/
cd .tmp/
curl -O https://fastdl.mongodb.org/osx/mongodb-osx-ssl-x86_64-4.0.4.tgz
tar -zxvf mongodb-osx-ssl-x86_64-4.0.4.tgz
sudo rm -rf /usr/local/mongodb
sudo mv mongodb-osx-x86_64-4.0.4/ /usr/local/mongodb
cd -
rm -rf .tmp/
sudo mkdir -p /data/db
sudo chown $USER /data/db
sudo chown $USER ~/.bash_profile
echo "export MONGO_PATH=/usr/local/mongodb" >> ~/.bash_profile
echo "export PATH=$PATH:/usr/local/mongodb/bin" >> ~/.bash_profile
source ~/.bash_profile



