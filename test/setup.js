global.should = require('chai').should();

const prepare = require('mocha-prepare');
const MongoMemoryServer = require('mongodb-memory-server').MongoMemoryServer;

const connectionManager = require('../server/core/connectionManager');

let mongoServer;
let onStart = done => {
  mongoServer = new MongoMemoryServer();

  mongoServer.getConnectionString().then((mongoUri) => {
    connectionManager.initializeDBUsingURI(mongoUri);
    done();
  });
};

let onFinish = done => {
  connectionManager.stopDB()
    .then(() => {
      mongoServer.stop();
      done();
    });
};

prepare(onStart, onFinish);
