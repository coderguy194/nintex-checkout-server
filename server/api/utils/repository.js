const mongoose = require('mongoose');

function getOneOrAll(dto) {
  let defaultResponse = dto.queryMethod === 'findOne' ? {} : [];

  let query = mongoose.model(dto.modelName)[dto.queryMethod](dto.queryData);
  query = dto.sort ? query.sort(dto.sort) : query;
  query = dto.limit ? query.limit(dto.limit) : query;
  query = dto.populate ? query.populate(dto.populate) : query;

  return new Promise((resolve, reject) => {
    query
      .exec((err, result) => {
        if (err) {
          return reject(err);
        }

        resolve(result || defaultResponse);
      });
  });
}

function save(existing, modelName, payload) {
  let objectToSave = existing;

  if (!existing) {
    let Model = mongoose.model(modelName);
    objectToSave = new Model(payload);
  }

  return new Promise((resolve, reject) => {
    objectToSave.save((err, saved) => {
      if (err) {
        return reject(err);
      }

      resolve(saved);
    });
  });

}

module.exports = {
  getOneOrAll,
  save
};
