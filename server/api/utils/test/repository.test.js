/* global should:true */
const Product = require('../../product').model;
const fixture = require('./fixtures/product');
const repository = require('../repository');

describe('Repository', () => {
  before(() => {
    fixture.products.forEach((product) => {
      let productObj = new Product(product);
      productObj.save();
    });
  });

  after(() => {
    Product.remove({});
  });

  describe('getOneOrAll', () => {
    it('should return all the results if no specific id is given', (done) => {
      repository.getOneOrAll('Product')
        .then((products) => {
          products.length.should.equal(2);
          done();
        });
    });

    it('should return specific results based on query data', (done) => {
      repository.getOneOrAll('Product', {'productId': 'wf'})
        .then((products) => {
          products.length.should.equal(1);
          products[0].productId.should.equal('wf');
          products[0].productName.should.equal('Workflow');
          done();
        });
    })
  });

  describe('save', () => {
    it('should create a new entry if no existing is provided', (done) => {
      repository.save(null, 'Product', {
        productId: 'dd',
        productName: 'Daredevil',
        price: 400
      }).then(() => {
        Product.findOne({productId: 'dd'})
          .exec()
          .then((product) => {
            product.productId.should.equal('dd');
            done();
          });
      });
    });

    it('should save an existing with updated data', (done) => {
      let confirmUpdate = () => {
        Product.findOne({productId: 'wf'})
          .then((returnedProduct) => {
            should.not.exist(returnedProduct);

            Product.findOne({productId: 'wf_mod'})
              .then((returnedProduct2) => {
                should.exist(returnedProduct2);
                returnedProduct2.productId.should.equal('wf_mod');
                done();
              });
          });
      };

      Product.findOne({productId: 'wf'})
        .exec()
        .then((product) => {
          product.productId = 'wf_mod';
          repository.save(product)
            .then(() => {
              confirmUpdate();
            });
        });
    })
  });
});
