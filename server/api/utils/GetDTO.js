function GetDTO() {
  this.modelName = '';
  this.queryMethod = 'find';
  this.queryData = {};
  this.populate = null;
  this.sort = null;
  this.limit = 10;
}

module.exports = GetDTO;
