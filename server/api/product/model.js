const mongoose = require('mongoose');

let ProductSchema = new mongoose.Schema({
  productId: {type: String, required: true},
  productName: {type: String, required: true},
  price: {type: Number}
});

module.exports = mongoose.model('Product', ProductSchema);
