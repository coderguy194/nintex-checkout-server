const express = require('express');
const statusCodes = require('http-status-codes');
const router = express.Router({mergeParams: true});

const controller = require('./controller');

router.get('/:productId?', function (req, res) {
  controller.get(req.params.productId)
    .then((result) => {
      res.status(statusCodes.OK).send(result);
    })
    .catch((err) => {
      res.status(statusCodes.NOT_FOUND).send(err);
    });
});

router.post('/', function (req, res) {
  controller.createOrUpdate(req.body)
    .then((result) => {
      res.status(statusCodes.CREATED).send(result);
    })
    .catch((err) => {
      res.status(statusCodes.INTERNAL_SERVER_ERROR).send({
        message: err.message
      });
    });
});

module.exports = router;
