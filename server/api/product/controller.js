const logger = require('../../core/logger');
const {repository, GetDTO} = require('../utils');

function get(productId) {
  let dto = new GetDTO();
  dto.modelName = 'Product';

  if (productId) {
    dto.queryData = {productId};
    dto.queryMethod = 'findOne';
  }

  return repository.getOneOrAll(dto);
}

function createOrUpdate(productPayload) {
  return repository.getOneOrAll(
    'Product',
    {productId: productPayload.productId},
    null,
    null,
    true
  ).then((product) => {
    if (JSON.stringify(product) === '{}') {
      logger.log(`Creating product with product id ${productPayload.productId}...`);
      return repository.save(null, 'Product', productPayload);
    }

    logger.log(`Found product with product id ${productPayload.productId}; updating...`);
    product.price = productPayload.price;
    product.productName = productPayload.productName;
    return repository.save(product);
  });
}

module.exports = {
  get,
  createOrUpdate
};
