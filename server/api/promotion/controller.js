const {repository, GetDTO} = require('../utils');

function get(promotionCode) {
  let dto = new GetDTO();
  dto.modelName = 'Promotion';
  dto.populate = 'conditions';
  if (promotionCode) {
    dto.queryData = {promotionCode};
    dto.queryMethod = 'findOne';
  }

  return repository.getOneOrAll(dto);
}

function create(promotionPayload) {
  let conditonData = promotionPayload.conditions;
  delete promotionPayload.conditions;

  return repository.save(null, 'Promotion', promotionPayload)
    .then((promotion) => {
      return _createConditionsForPromotion(conditonData, promotion)
        .then((updatedPromotion) => {
          return updatedPromotion;
        })
    });
}

function _createConditionsForPromotion(conditionList, promotion) {
  let creationPromises = [];
  conditionList.forEach((condition) => {
    condition.relatedPromotion = promotion._id;
    creationPromises.push(repository.save(null, 'Condition', condition));
  });

  return Promise.all(creationPromises)
    .then((conditions) => {
      promotion.conditions = conditions;
      return promotion.save()
        .then(updatePromotion => updatePromotion);
    });
}

module.exports = {
  get,
  create
};
