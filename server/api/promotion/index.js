module.exports = {
  route: require('./route'),
  models: {
    promotion: require('./models/promotion'),
    condition: require('./models/condition')
  }
};
