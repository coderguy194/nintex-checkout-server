const mongoose = require('mongoose');
const {ObjectId} = mongoose.Schema.Types;

const REDUCTION_TYPES = ['%', 'currency'];
const APPLY_ON_TYPES = ['totalAmount', 'unitPrice'];

let PromotionSchema = new mongoose.Schema({
  promotionCode: {type: String, required: true},
  applyOn: {
    type: String,
    required: true,
    enum: APPLY_ON_TYPES,
    default: APPLY_ON_TYPES[0]
  },
  relevantFor: {
    type: ObjectId,
    ref: 'Product'
  },
  flatReduction: {
    amount: {type: Number, default: 0},
    by: {
      type: String,
      enum: REDUCTION_TYPES,
      default: REDUCTION_TYPES[0]
    }
  },
  unitPriceReduction: {
    amount: {type: Number, default: 0},
    by: {
      type: String,
      enum: REDUCTION_TYPES,
      default: REDUCTION_TYPES[0]
    }
  },
  conditions: [
    {
      type: ObjectId,
      ref: 'Condition'
    }
  ]
});

module.exports = mongoose.model('Promotion', PromotionSchema);
