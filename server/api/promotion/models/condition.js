const mongoose = require('mongoose');
const {ObjectId} = mongoose.Schema.Types;
const CONDITION_TYPES = ['priceAmount', 'itemCount'];
const CLAUSE_TYPES = ['>', '>='];

let ConditionSchema = new mongoose.Schema({
  relatedPromotion: {
    type: ObjectId,
    ref: 'Promotion',
    required: true
  },
  conditionType: {
    type: String,
    enum: CONDITION_TYPES,
    default: CONDITION_TYPES[0]
  },
  conditionAmount: {type: Number, default: 0},
  clause: {
    type: String,
    enum: CLAUSE_TYPES,
    default: CLAUSE_TYPES[0]
  }
});

module.exports = mongoose.model('Condition', ConditionSchema);
