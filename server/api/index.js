const express = require('express');
const router = express.Router();
const product = require('./product');
const promotion = require('./promotion');

router.use('/products', product.route);
router.use('/promotions', promotion.route);

module.exports = router;
