module.exports = {
  SERVER_NAME: 'nintexCheckoutServer',
  DEFAULT_PORT: 8282,
  MONGO_URL: '127.0.0.1:27017',
  MONGO_DB_NAME: 'nintex-checkout'
};
