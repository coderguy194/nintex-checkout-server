### Nintex Checkout Server

The product checkout microservice.

This is built using my open source node [boilerplate](https://github.com/dibosh/simple-node-server)

#### Setup

- Install dependencies: `npm install`

- Install mongodb: `. ./setup/install-mongo.sh`

- Create a new terminal window and type `mongod` to run the mongo server

#### Running the Project

- This starts the server in port `8282`: `npm start`

- To run in different port: `PORT=8000 npm start`

#### Serving the Client/Frontend 

```
CLIENT_DIR=<absolute path of nintex-checkout-client app dir> npm start
```

#### The Structure

``` 
index.js <-- entry point for node
server <-- root folder for all functionalities
| - core <-- all the core functionalities belong here 
  | - shutdownManager.js 
  | - bootstrapper.js
  | - ...
  
| - config <-- all server specific config resides here
  | - index.js
  
| - api <-- all the api resources belong here
  | - index.js <-- any new api resource is registered here
  
  | - <resource>
    | - route.js <-- specific routing for this resource e.g. /user/all, /user/auth etc.
    | - controller.js <-- controller for this resource, business and db access logics should
    reside here. Feel free to split up db access layer into a repository if you want
    | - repository.json <-- if you separate out db access codes from controller.js
    | - promotion.js <-- if this resource needs a model schema to be specified
    | - index.js <-- entry point for model, controller, repository and route for this resource
    | - test
      | - <type>.test.js <-- tests for this resource; type can be model, repository, controller
      etc. For each there should be one separate test file
      
| - testHelpers <-- test specific helpers, shared mocks, global vars should be here
  | - ...    
```

#### Available APIs

- base: `/api`

- `/products`

    - `POST`
    
        - Creates a new product
    
        - Sample Request
        
        ``` 
        {
          productId: 'wf',
          productName: 'Workflow',
          price: 11
        }
        ```
    
    - `GET`
  
        - Returns all the products
    
    - `GET` `/products/productId`
  
        - Returns a single product with given product id
    
- `/promotions`

    - `POST`
  
        - Creates a new promotion rule
    
        - Sample Request
        
        ``` 
        {
          promotionCode: 'BKSSL09',
          applyOn: 'totalAmount',
          flatReduction: {
            amount: 15,
            by: '%'
          },
          unitPriceReduction: {
            amount: 0,
            type: '%'
          },
          conditions: [
            {
              conditionType: 'priceAmount',
              conditionAmount: 1500,
              clause: '>='
            }
          ]
        }
        ```
        
    - `GET`
      
        - Get a list of all the available promotions
        
    - `GET` `/promotions/promotionCode`
      
        - Returns a single promotion with given promo code
 

#### Other Important Commands

- To run tests: `npm run test`

- To check for lint errors: `npm run lint`

